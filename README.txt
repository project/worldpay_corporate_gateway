Readme
================================================================================

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
================================================================================
This module helps to integrate your site to WorldPay Corporate Gateway account.
It uses hosted or direct integration with Worldpay.


Features Include:

    1.  Option to choose between Direct vs Hosted (redirect) payments
    2.  Settings page where you can enter your WorldPay Corporate Gateway
        account credentials
    3.  Form where users can enter their data and submit to be redirected
        to WorldPay payment page
    4.  Crate a table in DB and records details of the transaction
    5.  Creates new orderID starting from 10001


REQUIREMENTS
================================================================================
Drupal 8.x
WorldPay - WPG Corporate Gateway Account


INSTALLATION
================================================================================
Setup
/admin/config/worldpay_corporate_gateway_settings/keys
Use
/billing/worldpay_payment/userdata

CONFIGURATION
================================================================================
To set up the module requires:
  1.  WPG URL to send XML messages to
      For prod use -
      https://secure.worldpay.com/jsp/merchant/xml/paymentService.jsp
      For test use -
      https://secure-test.worldpay.com/jsp/merchant/xml/paymentService.jsp
  2.  WPG New Username
      Log in to the test Merchant Administration Interface (MAI):
      https://secure-test.worldpay.com/sso/public/auth/login.html?serviceIdentifier=merchantadmin
      Click ACCOUNT, then select Profile from the top menu.
      Note your New Username for the connection.
  3.  WPG XML Password
      Log in to the test Merchant Administration Interface (MAI):
      https://secure-test.worldpay.com/sso/public/auth/login.html?serviceIdentifier=merchantadmin
      Click ACCOUNT, then select Profile from the top menu.
      Note your New Username for the connection.
      Click the pencil icon next to XML Password.
      Enter your new password and click Save XML Password.
  4.  Merchant Code
      Should get it from your implementation manager
  5.  Installation Id
      Should get it from your implementation manager
Note: Whitelist all IPs that will connect to WorldPay payment gateway.
Integration->Merchant Environment->New Test IP and add your IP
Google - "What's my IP"

Ref:
https://developer.worldpay.com/docs/wpg/hostedintegration/quickstart
https://developer.worldpay.com/docs/wpg/directintegration/quickstart
