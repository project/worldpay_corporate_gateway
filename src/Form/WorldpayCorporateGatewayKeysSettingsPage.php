<?php

namespace Drupal\worldpay_corporate_gateway\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Contains main function for settings.
 */
class WorldpayCorporateGatewayKeysSettingsPage extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'worldpay_corporate_gateway_settings_page';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['worldpay_corporate_gateway.settings'];
  }

  /**
   * Building form to save JSON URL.
   *
   * @param array $form
   *   Drupal form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Drupal FormStateInterface.
   *
   * @return array
   *   Returns the form to be displayed.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('worldpay_corporate_gateway.settings');
    $form['worldpay_corporate_gateway_method'] = [
      '#type' => 'select',
      '#title' => $this->t('WPG Integration method'),
      '#options' => [
        'direct' => $this->t('Worldpay (Direct)'),
        'redirect' => $this->t('Worldpay (Redirect)'),
      ],
      '#required' => TRUE,
      '#default_value' => $config->get('worldpay_corporate_gateway_method'),
    ];
    $form['worldpay_corporate_gateway_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('WPG URL to send XML messages to'),
      '#required' => TRUE,
      '#default_value' => $config->get('worldpay_corporate_gateway_url'),
    ];
    $form['worldpay_corporate_gateway_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('WPG New Username'),
      '#required' => TRUE,
      '#default_value' => $config->get('worldpay_corporate_gateway_username'),
    ];
    $form['worldpay_corporate_gateway_password'] = [
      '#type' => 'password',
      '#title' => $this->t('WPG XML Password'),
      '#required' => TRUE,
      '#attributes' => ['value' => $config->get('worldpay_corporate_gateway_password')],
    ];
    $form['worldpay_corporate_gateway_merchantcode'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant Code'),
      '#required' => TRUE,
      '#attributes' => ['value' => $config->get('worldpay_corporate_gateway_merchantcode')],
    ];
    $form['worldpay_corporate_gateway_installationid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Installation Id (only needed for Redirect)'),
      '#default_value' => $config->get('worldpay_corporate_gateway_installationid'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('worldpay_corporate_gateway.settings')
      ->set('worldpay_corporate_gateway_method', $values['worldpay_corporate_gateway_method'])
      ->set('worldpay_corporate_gateway_url', $values['worldpay_corporate_gateway_url'])
      ->set('worldpay_corporate_gateway_username', $values['worldpay_corporate_gateway_username'])
      ->set('worldpay_corporate_gateway_password', $values['worldpay_corporate_gateway_password'])
      ->set('worldpay_corporate_gateway_merchantcode', $values['worldpay_corporate_gateway_merchantcode'])
      ->set('worldpay_corporate_gateway_installationid', $values['worldpay_corporate_gateway_installationid'])
      ->save();
    parent::submitForm($form, $form_state);
  }

}
