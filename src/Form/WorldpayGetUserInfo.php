<?php

namespace Drupal\worldpay_corporate_gateway\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Locale\CountryManagerInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Defines a form that gets user data for payment.
 */
class WorldpayGetUserInfo extends ConfigFormBase {

  /**
   * The country manager.
   *
   * @var \Drupal\Core\Locale\CountryManagerInterface
   */
  protected $countryManager;

  /**
   * The country manager.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * User details.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Constructs a RegionalForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Locale\CountryManagerInterface $country_manager
   *   The country manager.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The logged in user details.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The IP details.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   A Guzzle client object.
   */
  public function __construct(ConfigFactoryInterface $config_factory,
                              CountryManagerInterface $country_manager,
                              Connection $database,
                              AccountProxyInterface $currentUser,
                              RequestStack $requestStack,
                              ClientInterface $http_client) {
    parent::__construct($config_factory);
    $this->countryManager = $country_manager;
    $this->database = $database;
    $this->currentUser = $currentUser;
    $this->httpClient = $http_client;
    $this->requestStack = $requestStack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('country_manager'),
      $container->get('database'),
      $container->get('current_user'),
      $container->get('request_stack'),
      $container->get('http_client'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'worldpay_corporate_gateway_settings_page';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['worldpay_corporate_gateway.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $countries = $this->countryManager->getList();
    $system_date = $this->config('system.date');
    $form['worldpay_corporate_gateway_country'] = [
      '#type' => 'select',
      '#title' => $this->t('Default country'),
      '#empty_value' => '',
      '#default_value' => $system_date->get('country.default'),
      '#options' => $countries,
      '#attributes' => ['class' => ['country-detect']],
    ];
    $form['worldpay_corporate_gateway_full_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Full Name'),
      '#required' => TRUE,
    ];
    $form['worldpay_corporate_gateway_address_1'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Address 1'),
      '#required' => TRUE,
    ];
    $form['worldpay_corporate_gateway_address_2'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Address 2'),
    ];
    $form['worldpay_corporate_gateway_city'] = [
      '#type' => 'textfield',
      '#title' => $this->t('City'),
      '#required' => TRUE,
    ];
    $form['worldpay_corporate_gateway_state'] = [
      '#type' => 'textfield',
      '#title' => $this->t('State'),
      '#required' => TRUE,
    ];
    $form['worldpay_corporate_gateway_zip_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Zip Code'),
      '#required' => TRUE,
    ];
    $config = $this->config('worldpay_corporate_gateway.settings');
    $worldpay_corporate_gateway_method = $config->get('worldpay_corporate_gateway_method');
    if ($worldpay_corporate_gateway_method == 'direct') {
      $form['worldpay_corporate_gateway_credit_card_number'] = [
        '#type' => 'creditfield_cardnumber',
        '#title' => $this->t('Credit Card Number'),
        '#attributes' => [
          'placeholder' => $this->t('1111222233334444'),
        ],
        '#maxlength' => 16,
      ];
      $form['worldpay_corporate_gateway_expiration_month'] = [
        '#type' => 'creditfield_expiration',
        '#title' => $this->t('Expiration Date (YYYY-MM)'),
        '#attributes' => [
          'placeholder' => $this->t('YYYY-MM'),
        ],
      ];
    }
    $form['worldpay_corporate_gateway_amount'] = [
      '#type' => 'number',
      '#step' => '.01',
      '#min' => '0',
      '#title' => $this->t('Amount'),
      '#required' => TRUE,
    ];
    $form['worldpay_corporate_gateway_currency'] = [
      '#markup' => '<p><b>Currency: USD</b></p>',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Pay using WorldPay'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config = $this->config('worldpay_corporate_gateway.settings');
    $worldpay_corporate_gateway_method = $config->get('worldpay_corporate_gateway_method');
    $worldpay_corporate_gateway_url = $config->get('worldpay_corporate_gateway_url');
    $worldpay_corporate_gateway_username = $config->get('worldpay_corporate_gateway_username');
    $worldpay_corporate_gateway_password = $config->get('worldpay_corporate_gateway_password');
    $worldpay_corporate_gateway_merchantcode = $config->get('worldpay_corporate_gateway_merchantcode');
    $worldpay_corporate_gateway_installationid = $config->get('worldpay_corporate_gateway_installationid');
    $worldpay_corporate_gateway_currentUserEmail = $this->currentUser->getEmail();
    $worldpay_corporate_gateway_ip = $this->requestStack->getClientIp();
    $message = $this->messenger();
    if ((!isset($worldpay_corporate_gateway_method))
    || (!isset($worldpay_corporate_gateway_url))
    || (!isset($worldpay_corporate_gateway_username))
    || (!isset($worldpay_corporate_gateway_password))
    || (!isset($worldpay_corporate_gateway_merchantcode))
    ) {
      $message->addError($this->t('Error:: Unable to make payment.
      Please add keys in WorldPay - WPG Corporate Gateway
      module configuration.'));
      return;
    }
    if (($worldpay_corporate_gateway_method == "redirect")
    && ($worldpay_corporate_gateway_installationid == "")
    ) {
      $message->addError($this->t('Error:: Unable to make payment.
      Please add InstallationID in WorldPay - WPG Corporate Gateway
      module configuration.'));
      return;
    }
    $table_values = [
      [
        'user_id' => $worldpay_corporate_gateway_currentUserEmail,
        'ip_address' => $worldpay_corporate_gateway_ip,
        'full_name' => $values['worldpay_corporate_gateway_full_name'],
        'address_1' => $values['worldpay_corporate_gateway_address_1'],
        'address_2' => $values['worldpay_corporate_gateway_address_2'],
        'city' => $values['worldpay_corporate_gateway_city'],
        'state' => $values['worldpay_corporate_gateway_state'],
        'zip' => $values['worldpay_corporate_gateway_zip_code'],
        'country' => $values['worldpay_corporate_gateway_country'],
        'amount' => $values['worldpay_corporate_gateway_amount'],
        'currency' => 'USD',
      ],
    ];
    $query = $this->database->insert('worldpay_corporate_gateway_transactions')
      ->fields([
        'user_id',
        'ip_address',
        'full_name',
        'address_1',
        'address_2',
        'city',
        'state',
        'zip',
        'country',
        'amount',
        'currency',
      ]);
    foreach ($table_values as $record) {
      $query->values($record);
    }
    $order_id = $query->execute();
    $amount = (int) ($values['worldpay_corporate_gateway_amount'] * 100);
    if ($worldpay_corporate_gateway_method == "redirect") {
      $this->wpgRedirect($values, $amount,
        $worldpay_corporate_gateway_merchantcode,
        $order_id, $worldpay_corporate_gateway_installationid,
        $worldpay_corporate_gateway_currentUserEmail,
        $worldpay_corporate_gateway_username,
        $worldpay_corporate_gateway_password,
        $worldpay_corporate_gateway_url);
    }
    if ($worldpay_corporate_gateway_method == "direct") {
      $this->wpgDirect($values, $amount,
        $worldpay_corporate_gateway_merchantcode,
        $order_id,
        $worldpay_corporate_gateway_currentUserEmail,
        $worldpay_corporate_gateway_username,
        $worldpay_corporate_gateway_password,
        $worldpay_corporate_gateway_url,
        $worldpay_corporate_gateway_ip);
    }
  }

  /**
   * To handle redirect payment.
   */
  private function wpgRedirect($values,
                               $amount,
                               $worldpay_corporate_gateway_merchantcode,
                               $order_id,
                               $worldpay_corporate_gateway_installationid,
                               $worldpay_corporate_gateway_currentUserEmail,
                               $worldpay_corporate_gateway_username,
                               $worldpay_corporate_gateway_password,
                               $worldpay_corporate_gateway_url) {
    $request_xml = '<?xml version="1.0" encoding="UTF-8"?>
    <!DOCTYPE paymentService PUBLIC "-//Worldpay//DTD Worldpay PaymentService v1//EN" "http://dtd.worldpay.com/paymentService_v1.dtd">
    <paymentService version="1.4" merchantCode="' . $worldpay_corporate_gateway_merchantcode . '">
       <submit>
          <order orderCode="' . $order_id . '" installationId="' . $worldpay_corporate_gateway_installationid . '">
             <description>Payment from ' . $values['worldpay_corporate_gateway_full_name'] . ' Order ID - ' . $order_id . '</description>
             <amount currencyCode="USD" exponent="2" value="' . $amount . '" />
             <orderContent><![CDATA[Using Drupal WorldPay WPG Corporate Gateway module for payment]]></orderContent>
             <paymentMethodMask>
                <include code="ALL" />
             </paymentMethodMask>
             <shopper>
                <shopperEmailAddress>' . $worldpay_corporate_gateway_currentUserEmail . '</shopperEmailAddress>
             </shopper>
             <billingAddress>
                <address>
                   <address1>' . $values['worldpay_corporate_gateway_address_1'] . '</address1>
                   <address2>' . $values['worldpay_corporate_gateway_address_2'] . '</address2>
                   <postalCode>' . $values['worldpay_corporate_gateway_zip_code'] . '</postalCode>
                   <city>' . $values['worldpay_corporate_gateway_city'] . '</city>
                   <state>' . $values['worldpay_corporate_gateway_state'] . '</state>
                   <countryCode>' . $values['worldpay_corporate_gateway_country'] . '</countryCode>
                </address>
             </billingAddress>
          </order>
       </submit>
    </paymentService>';
    $options = [
      'auth' => [
        $worldpay_corporate_gateway_username,
        $worldpay_corporate_gateway_password,
      ],
      'timeout' => 30,
      'body' => $request_xml,
    ];
    $message = $this->messenger();
    try {
      $response = $this->httpClient
        ->request('GET',
          $worldpay_corporate_gateway_url,
          $options);
    }
    catch (GuzzleException $ex) {
      $message->addError($this->t('Error:: Unable to connect to Worldpay
      payment gateway.'));
      watchdog_exception('worldpay_corporate_gateway', $ex);
      return;
    }
    $xml_response = $response->getBody()->getContents();
    $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $xml_response);
    $xml = simplexml_load_string($xml);
    $json = json_encode($xml);
    $responseArray = json_decode($json, TRUE);
    if (isset($responseArray['reply']["orderStatus"]["reference"])) {
      $response = new RedirectResponse($responseArray['reply']["orderStatus"]["reference"]);
      $response->send();
      return;
    }
    else {
      $message->addError($this->t('Error:: Unable to redirect to Worldpay
      payment gateway.'));
    }
  }

  /**
   * To handle direct payment.
   */
  private function wpgDirect($values,
                             $amount,
                             $worldpay_corporate_gateway_merchantcode,
                             $order_id,
                             $worldpay_corporate_gateway_currentUserEmail,
                             $worldpay_corporate_gateway_username,
                             $worldpay_corporate_gateway_password,
                             $worldpay_corporate_gateway_url,
                             $worldpay_corporate_gateway_ip) {
    $dateparts = explode('-', $values['worldpay_corporate_gateway_expiration_month']);
    $year = (int) $dateparts[0];
    $month = (int) $dateparts[1];
    $request_xml = '<?xml version="1.0" encoding="UTF-8"?>
    <!DOCTYPE paymentService PUBLIC "-//Worldpay//DTD Worldpay PaymentService v1//EN" "http://dtd.worldpay.com/paymentService_v1.dtd">
    <paymentService version="1.4" merchantCode="' . $worldpay_corporate_gateway_merchantcode . '">
       <submit>
          <order orderCode="' . $order_id . '">
             <description>Payment from ' . $values['worldpay_corporate_gateway_full_name'] . ' Order ID - ' . $order_id . '</description>
             <amount currencyCode="USD" exponent="2" value="' . $amount . '" />
             <paymentDetails>
                <CARD-SSL>
                   <cardNumber>' . $values['worldpay_corporate_gateway_credit_card_number'] . '</cardNumber>
                   <expiryDate>
                      <date month="' . $month . '" year="' . $year . '" />
                   </expiryDate>
                   <cardHolderName>' . $values['worldpay_corporate_gateway_full_name'] . '</cardHolderName>
                   <cardAddress>
                      <address>
                         <address1>' . $values['worldpay_corporate_gateway_address_1'] . '</address1>
                         <address2>' . $values['worldpay_corporate_gateway_address_2'] . '</address2>
                         <postalCode>' . $values['worldpay_corporate_gateway_zip_code'] . '</postalCode>
                         <city>' . $values['worldpay_corporate_gateway_city'] . '</city>
                         <state>' . $values['worldpay_corporate_gateway_state'] . '</state>
                         <countryCode>' . $values['worldpay_corporate_gateway_country'] . '</countryCode>
                      </address>
                   </cardAddress>
                </CARD-SSL>
                <session shopperIPAddress="' . $worldpay_corporate_gateway_ip . '" id="' . bin2hex(random_bytes(8)) . '" />
             </paymentDetails>
             <shopper>
                <shopperEmailAddress>' . $worldpay_corporate_gateway_currentUserEmail . '</shopperEmailAddress>
                <browser>
                   <acceptHeader>text/html</acceptHeader>
                   <userAgentHeader>' . $_SERVER['HTTP_USER_AGENT'] . '</userAgentHeader>
                </browser>
             </shopper>
          </order>
       </submit>
    </paymentService>';
    $options = [
      'auth' => [
        $worldpay_corporate_gateway_username,
        $worldpay_corporate_gateway_password,
      ],
      'timeout' => 30,
      'body' => $request_xml,
    ];
    $message = $this->messenger();
    try {
      $response = $this->httpClient
        ->request('GET',
          $worldpay_corporate_gateway_url,
          $options);
    }
    catch (GuzzleException $ex) {
      $message->addError($this->t('Error:: Unable to connect to Worldpay
      payment gateway.'));
      watchdog_exception('worldpay_corporate_gateway', $ex);
      return;
    }
    $xml_response = $response->getBody()->getContents();
    $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $xml_response);
    $xml = simplexml_load_string($xml);
    $json = json_encode($xml);
    $responseArray = json_decode($json, TRUE);
    if (isset($responseArray['reply']["orderStatus"]["payment"]["lastEvent"])) {
      if ($responseArray['reply']["orderStatus"]["payment"]["lastEvent"] == "AUTHORISED") {
        $message->addStatus('Your Payment was Successful');
      }
      else {
        $message->addError('Your Payment was NOT Successful');
      }
    }
    else {
      $message->addError($this->t('Error:: Unable to redirect to Worldpay
      payment gateway.'));
    }
  }

}
